﻿Fab's Source Armor Pierce Shots Fix!
This is indeed a very easy fix, but make sure you don't have any corefiles Edited because you will Need to find the lines then.
So lets Get Started!

First off Navigate to wServer/realm/entities/player/player.cs

Find 
void Damage(int dmg, Entity chr);
(line 18)

and Replace it with 
void Damage(int dmg, Entity chr, Boolean NoDef);
After you have done that Search the same file for
public void Damage(int dmg, Entity chr)
(line 284)
and Replace with
public void Damage(int dmg, Entity chr, Boolean NoDef)
After that, Search again in the same file
dmg = (int)StatsManager.GetDefenseDamage(dmg, false);
(line 293)

Replace with
dmg = (int)StatsManager.GetDefenseDamage(dmg, NoDef);

So. Thats it for Player.cs now. now we navigate to wServer/networking/handlers/PlayerHitHandler.cs

and find
client.Player.Damage(proj.Damage, proj.ProjectileOwner.Self);
(Around line 44)

Replace With
client.Player.Damage(proj.Damage, proj.ProjectileOwner.Self, proj.Descriptor.ArmorPiercing);

Now we Navigate to wServer/logic/behaviors/Grenade.cs

Search for :
world.Aoe(target, radius, true, p => { (p as IPlayer).Damage(damage, host as Character); });
(Around Line 80)

Replace with
world.Aoe(target, radius, true, p => { (p as IPlayer).Damage(damage, host as Character, false); });


Now we Navigate to wServer/realm/entities/Decoy.cs

And Search for 
public void Damage(int dmg, Entity chr) { }
(Around line 44)

And Replace with
public void Damage(int dmg, Entity chr, Boolean NoDef) { }


And now the Final Part. Goto wServer/realm/entities/Pet.cs

and Find
Code:
public void Damage(int dmg, Entity chr) { }
(line 392)

And Replace with
public void Damage(int dmg, Entity chr, Boolean NoDef) { }