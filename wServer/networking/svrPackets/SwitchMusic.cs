﻿using db;

namespace wServer.networking.svrPackets
{
    public class SwitchMusic : ServerPacket
    {
        public string Music { get; set; }

        public override PacketID ID
        {
            get { return PacketID.SWITCH_MUSIC; }
        }

        public override Packet CreateInstance()
        {
            return new SwitchMusic();
        }
        

        protected override void Read(Client psr, NReader rdr)
        {
            Music = rdr.ReadUTF();
        }

        protected override void Write(Client psr, NWriter wtr)
        {
            wtr.WriteUTF(Music);
        }
    }
}