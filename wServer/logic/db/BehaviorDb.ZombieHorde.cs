﻿#region "Internet Providers"
using wServer.logic.behaviors;
using wServer.logic.transitions;
using wServer.logic.loot;
#endregion

// Curse of the Unholy 2018 (c) (Unholy Realms v7 2016-2018)

namespace wServer.logic
{
    partial class BehaviorDb
    {
        private _ ZombieHordeRealm = () => Behav()

        .Init("Zombie Horde Event",
                new State(
                    new StayCloseToSpawn(0.5, 10),
                    new State("Hm",
                        new PlayerWithinTransition(15, "GGX"),
                        new ConditionalEffect(ConditionEffectIndex.Invincible)
                        ),
                    new State("GGX",
                        new EntityNotExistsTransition("Zombie Horde Spawner", 10, "intro"),
                        new ConditionalEffect(ConditionEffectIndex.Invincible)
                        ),
                    new State("intro",
                            new Flash(0xfFF0000, 0.5, 9000001),
                            new Taunt("Mortal scum! I shall not fall to your puny weapons! With or without flesh, I shall be your end!"),
                            new TimedTransition(6000, "protect"),
                        new ConditionalEffect(ConditionEffectIndex.Invincible)
                        ),
                    new State("protect",
                        new Remeff(ConditionEffectIndex.Invincible),
                        new Shoot(8, projectileIndex: 0, predictive: 0.25, coolDown: 2500, count: 2, shootAngle: 50),
                        new Shoot(8, projectileIndex: 0, predictive: 0.35, coolDown: 2800, count: 3, shootAngle: 5),
                        new Shoot(8, projectileIndex: 1, predictive: 0.25, coolDown: 1700, count: 6),
                        new Shoot(8, projectileIndex: 2, predictive: 0.25, coolDown: 3200, count: 3),
                        new Shoot(8, projectileIndex: 3, predictive: 0.25, coolDown: 4000, coolDownOffset: 800, count: 1),
                        new Shoot(8, projectileIndex: 3, predictive: 0.25, coolDown: 4000, coolDownOffset: 1600, count: 3, shootAngle: 50),
                        new Shoot(8, projectileIndex: 3, predictive: 0.25, coolDown: 4000, coolDownOffset: 2400, count: 5, shootAngle: 50),
                        new Shoot(8, projectileIndex: 3, predictive: 0.25, coolDown: 4000, coolDownOffset: 3200, count: 3, shootAngle: 50),
                        new Reproduce("Giant Skeleton", 10, 2, 1, 5000),
                        new HpLessTransition(0.5, "attacktrans"),
                        new Prioritize(
                            new Follow(0.5, 10.5, 4),
                            new Wander(0.4)
                            )
                        ),
                    new State("attacktrans",
                            new SetAltTexture(1),
                            new ConditionalEffect(ConditionEffectIndex.Invincible),
                            new Flash(0xfFF0000, 0.1, 9000001),
                            new Taunt("Grr... You may have charred my body, but my soul still remains!"),
                            new TimedTransition(5000, "attack")
                        ),
                    new State("attack",
                        new Order(15, "Giant Skeleton", "die"),
                        new Shoot(8, projectileIndex: 3, predictive: 0.25, coolDown: 300, count: 2, shootAngle: 50),
                        new Shoot(8, projectileIndex: 2, predictive: 0.35, coolDown: 2200, count: 3, shootAngle: 5),
                        new Shoot(8, projectileIndex: 4, predictive: 0.25, coolDown: 800, count: 6),
                        new Shoot(8, projectileIndex: 2, predictive: 0.25, coolDown: 1900, count: 3),
                        new Shoot(8, projectileIndex: 4, predictive: 0.25, coolDown: 1800, coolDownOffset: 450, count: 1),
                        new Shoot(8, projectileIndex: 4, predictive: 0.25, coolDown: 1800, coolDownOffset: 900, count: 3, shootAngle: 50),
                        new Shoot(8, projectileIndex: 5, predictive: 0.25, coolDown: 1800, coolDownOffset: 1350, count: 5, shootAngle: 50),
                        new Shoot(8, projectileIndex: 4, predictive: 0.25, coolDown: 1800, coolDownOffset: 1800, count: 3, shootAngle: 50),
                        new Prioritize(
                            new Follow(1, 10.5, 4),
                            new Wander(0.1)
                            )
                        )
                    ),
                new Threshold(0.1,
                    new ItemLoot("Candy Corn", 1),
                    new ItemLoot("Candy Corn", 1),
                    new ItemLoot("Potion of Life", 0.5)
                ),
                new MostDamagers(8,
                    new TierLoot(12, ItemType.Armor, 0.12),
                    new TierLoot(11, ItemType.Weapon, 0.12),
                    new TierLoot(5, ItemType.Ring, 0.05),
                    new TierLoot(5, ItemType.Ability, 0.05)
                ),
                new MostDamagers(2,
                    new ItemLoot("Infected Skin", 0.19),
                    new ItemLoot("Charred Skull of Halloween", 0.012),
                    new ItemLoot("Robe of Hallow Ember", 0.01)
                ),
                new Threshold(0.03,
                    new ItemLoot("Corrupted Cleaver", 0.6),
                    new ItemLoot("Staff of Horrific Knowledge", 0.6),
                    new ItemLoot("Dagger of the Terrible Talon", 061),
                    new ItemLoot("Skull-splitter Sword", 0.6),
                    new ItemLoot("Wand of Ancient Terror", 0.6),
                    new ItemLoot("Bow of Nightmares", 0.6)
                )
        )
        .Init("Giant Skeleton",
                new State(
                    new Prioritize(
                        new Follow(1, range: 7),
                        new Wander(0.2)
                        ),
                    new Protect(0.2, "Zombie Horde Event", 10, 10),
                        new Shoot(8, projectileIndex: 2, predictive: 0.25, coolDown: 100, count: 1),
                        new Shoot(8, projectileIndex: 0, predictive: 0.25, coolDown: 4000, coolDownOffset: 900, count: 1, shootAngle: 25),
                        new Shoot(8, projectileIndex: 0, predictive: 0.25, coolDown: 4000, coolDownOffset: 1800, count: 2, shootAngle: 25),
                        new Shoot(8, projectileIndex: 1, predictive: 0.25, coolDown: 4000, coolDownOffset: 2700, count: 3, shootAngle: 25),
                        new Shoot(8, projectileIndex: 0, predictive: 0.25, coolDown: 4000, coolDownOffset: 3600, count: 2, shootAngle: 25)
                    )
            )

        .Init("Zombie Horde Spawner",
            new State(
                new ConditionalEffect(ConditionEffectIndex.Invincible, true),
                new State("CPY1",
                    new StayCloseToSpawn(1, 4),
                    new Wander(0.5),
                    new TimedTransition(2000, "CPY2") //It's been shitting me so I added this
                    ),
                new State("CPY2",
                    new PlayerWithinTransition(40, "Choose")
                    ),
                new State("Choose",  //In order zombie classes 1-14
                    new TimedTransition(1000, "Choosen1", true),
                    new TimedTransition(1000, "Choosen2", true),
                    new TimedTransition(1000, "Choosen3", true),
                    new TimedTransition(1000, "Choosen4", true)
                    ),
                new State("Choosen1",
                    new TimedTransition(10000, "Choosen11"),
                    new Reproduce("Zombie Wizard", 10, 2, 1, coolDown: 2500), //1
                    new Reproduce("Zombie Mystic", 10, 2, 1, coolDown: 2500), //2
                    new Reproduce("Zombie Archer", 10, 2, 1, coolDown: 2500), //3
                    new Reproduce("Zombie Priest", 10, 2, 1, coolDown: 2500) //12
                    ),
                new State("Choosen11",
                    new EntitiesNotExistsTransition(15, "die", "Zombie Wizard", "Zombie Mystic", "Zombie Archer") //No priest because priest is duplicate 
                ),
                new State("Choosen2",
                    new TimedTransition(10000, "Choosen21"),
                    new Reproduce("Zombie Knight", 10, 2, 1, coolDown: 2500), //4
                    new Reproduce("Zombie Warrior", 10, 2, 1, coolDown: 2500), //5
                    new Reproduce("Zombie Paladin", 10, 2, 1, coolDown: 2500), //6
                    new Reproduce("Zombie Huntress", 10, 2, 1, coolDown: 2500) //7
                    ),
                new State("Choosen21",
                    new EntitiesNotExistsTransition(15, "die", "Zombie Knight", "Zombie Warrior", "Zombie Paladin", "Zombie Huntress")
                ),
                new State("Choosen3",
                    new TimedTransition(10000, "Choosen31"),
                    new Reproduce("Zombie Necromancer", 10, 4, 1, coolDown: 2500), //8
                    new Reproduce("Zombie Ninja", 10, 2, 1, coolDown: 2500), //9
                    new Reproduce("Zombie Sorcerer", 10, 2, 1, coolDown: 2500) //10
                    ),
                new State("Choosen31",
                    new EntitiesNotExistsTransition(15, "die", "Zombie Necromancer", "Zombie Ninja", "Zombie Sorcerer")
                ),
                new State("Choosen4",
                    new TimedTransition(10000, "Choosen41"),
                    new Reproduce("Zombie Rogue", 10, 2, 1, coolDown: 2500), //11
                    new Reproduce("Zombie Priest", 10, 2, 1, coolDown: 2500), //12
                    new Reproduce("Zombie Trickster", 10, 2, 1, coolDown: 2500), //13
                    new Reproduce("Zombie Assassin", 10, 2, 1, coolDown: 2500) //14
                    ),
                new State("Choosen41",
                    new EntitiesNotExistsTransition(15, "die", "Zombie Rogue", "Zombie Trickster", "Zombie Assassin") //No priest because priest is duplicate
                    ),
                new State("die",
                    new Suicide()
                    )
                )
            )

        .Init("Zombie Archer",
            new State(
                    new StayCloseToSpawn(1, 4),
                    new Wander(0.4), //forgot this till late in development, they will move in all phases so why not have some efficiency?
                new ConditionalEffect(ConditionEffectIndex.Armored, true),
                new State("RNG",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, false), //just in case, I don't code since some time
                    new TimedTransition(100, "Gx", true),
                    new TimedTransition(100, "G", true)
                    ),
                new State("Gx",
                    new Taunt(0.1, 15000,
                        "GGHHhh... Rghh!",
                        "ARRghhhh",
                        "Rwarrhg!!"
                        ),
                    new Shoot(7, 1, coolDown: 1800)
                    ),
                new State("G",
                    new Shoot(7, 1, coolDown: 2000)
                 )
                    ),
                new MostDamagers(1,
                    new ItemLoot("Infected Skin", 0.001)
                ),
                new OnlyOne(
                    new ItemLoot("Corrupted Cleaver", 0.05),
                    new ItemLoot("Staff of Horrific Knowledge", 0.05),
                    new ItemLoot("Dagger of the Terrible Talon", 0.05),
                    new ItemLoot("Skull-splitter Sword", 0.05),
                    new ItemLoot("Wand of Ancient Terror", 0.05),
                    new ItemLoot("Bow of Nightmares", 0.05)
                ),
                new Threshold(0.1,
                    new ItemLoot("Candy Corn", 0.1)
                )
      )

        .Init("Zombie Warrior",
            new State(
                    new StayCloseToSpawn(1, 4),
                    new Wander(0.4),
                new ConditionalEffect(ConditionEffectIndex.Armored, true),
                new State("RNG",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, false),
                    new TimedTransition(100, "Gx", true),
                    new TimedTransition(100, "G", true)
                    ),
                new State("Gx", 
                    new Taunt(0.1, 15000,
                        "GGHHhh... Rghh!",
                        "ARRghhhh",
                        "Rwarrhg!!"
                        ),
                    new Shoot(5, 1, coolDown: 1800)
                    ),
                new State("G",
                    new Shoot(5, 1, coolDown: 2000)
                 )
                    ),
                new MostDamagers(1,
                    new ItemLoot("Infected Skin", 0.001)
                ),
                new OnlyOne(
                    new ItemLoot("Corrupted Cleaver", 0.05),
                    new ItemLoot("Staff of Horrific Knowledge", 0.05),
                    new ItemLoot("Dagger of the Terrible Talon", 0.05),
                    new ItemLoot("Skull-splitter Sword", 0.05),
                    new ItemLoot("Wand of Ancient Terror", 0.05),
                    new ItemLoot("Bow of Nightmares", 0.05)
                ),
                new Threshold(0.1,
                    new ItemLoot("Candy Corn", 0.1)
                )
      )

        .Init("Zombie Wizard",
            new State(
                    new StayCloseToSpawn(1, 4),
                    new Wander(0.4),
                new ConditionalEffect(ConditionEffectIndex.Armored, true),
                new State("RNG",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, false),
                    new TimedTransition(100, "Gx", true),
                    new TimedTransition(100, "G", true)
                    ),
                new State("Gx",
                    new Taunt(0.1, 15000,
                        "GGHHhh... Rghh!",
                        "ARRghhhh",
                        "Rwarrhg!!"
                        ),
                    new Shoot(10, 2, 5, coolDown: 1800)
                    ),
                new State("G",
                    new Shoot(10, 2, 5, coolDown: 2000)
                 )
                    ),
                new MostDamagers(1,
                    new ItemLoot("Infected Skin", 0.001)
                ),
                new OnlyOne(
                    new ItemLoot("Corrupted Cleaver", 0.05),
                    new ItemLoot("Staff of Horrific Knowledge", 0.05),
                    new ItemLoot("Dagger of the Terrible Talon", 0.05),
                    new ItemLoot("Skull-splitter Sword", 0.05),
                    new ItemLoot("Wand of Ancient Terror", 0.05),
                    new ItemLoot("Bow of Nightmares", 0.05)
                ),
                new Threshold(0.1,
                    new ItemLoot("Candy Corn", 0.1)
                )
      )

        .Init("Zombie Rogue",
            new State(
                    new StayCloseToSpawn(1, 4),
                    new Wander(0.4),
                new ConditionalEffect(ConditionEffectIndex.Armored, true),
                new State("RNG",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, false),
                    new TimedTransition(100, "Gx", true),
                    new TimedTransition(100, "G", true)
                    ),
                new State("Gx",
                    new Taunt(0.1, 15000,
                        "GGHHhh... Rghh!",
                        "ARRghhhh",
                        "Rwarrhg!!"
                        ),
                    new Shoot(6, 1, coolDown: 1800)
                    ),
                new State("G",
                    new Shoot(6, 1, coolDown: 2000)
                 )
                    ),
                new MostDamagers(1,
                    new ItemLoot("Infected Skin", 0.001)
                ),
                new OnlyOne(
                    new ItemLoot("Corrupted Cleaver", 0.05),
                    new ItemLoot("Staff of Horrific Knowledge", 0.05),
                    new ItemLoot("Dagger of the Terrible Talon", 0.05),
                    new ItemLoot("Skull-splitter Sword", 0.05),
                    new ItemLoot("Wand of Ancient Terror", 0.05),
                    new ItemLoot("Bow of Nightmares", 0.05)
                ),
                new Threshold(0.1,
                    new ItemLoot("Candy Corn", 0.1)
                )
      )

        .Init("Zombie Priest",
            new State(
                    new StayCloseToSpawn(1, 4),
                    new Wander(0.4),
                new ConditionalEffect(ConditionEffectIndex.Armored, true),
                new State("RNG",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, false),
                    new TimedTransition(100, "Gx", true),
                    new TimedTransition(100, "G", true)
                    ),
                new State("Gx",
                    new SpecificHeal(10, 300, "Zombies", 3500),
                    new Taunt(0.1, 15000,
                        "GGHHhh... Rghh!",
                        "ARRghhhh",
                        "Rwarrhg!!"
                        ),
                    new Shoot(10, 1, coolDown: 1800)
                    ),
                new State("G",
                    new SpecificHeal(10, 300, "Zombies", 3500), 
                    new Shoot(10, 1, coolDown: 2000)
                 )
                    ),
                new MostDamagers(1,
                    new ItemLoot("Infected Skin", 0.001)
                ),
                new OnlyOne(
                    new ItemLoot("Corrupted Cleaver", 0.05),
                    new ItemLoot("Staff of Horrific Knowledge", 0.05),
                    new ItemLoot("Dagger of the Terrible Talon", 0.05),
                    new ItemLoot("Skull-splitter Sword", 0.05),
                    new ItemLoot("Wand of Ancient Terror", 0.05),
                    new ItemLoot("Bow of Nightmares", 0.05)
                ),
                new Threshold(0.1,
                    new ItemLoot("Candy Corn", 0.1)
                )
      )

        .Init("Zombie Knight",
            new State(
                    new StayCloseToSpawn(1, 4),
                    new Wander(0.4),
                new ConditionalEffect(ConditionEffectIndex.Armored, true),
                new State("RNG",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, false),
                    new TimedTransition(100, "Gx", true),
                    new TimedTransition(100, "G", true)
                    ),
                new State("Gx",
                    new Taunt(0.1, 15000,
                        "GGHHhh... Rghh!",
                        "ARRghhhh",
                        "Rwarrhg!!"
                        ),
                    new Shoot(5, 1, coolDown: 1800)
                    ),
                new State("G",
                    new Shoot(5, 1, coolDown: 2000)
                 )
                    ),
                new MostDamagers(1,
                    new ItemLoot("Infected Skin", 0.001)
                ),
                new OnlyOne(
                    new ItemLoot("Corrupted Cleaver", 0.05),
                    new ItemLoot("Staff of Horrific Knowledge", 0.05),
                    new ItemLoot("Dagger of the Terrible Talon", 0.05),
                    new ItemLoot("Skull-splitter Sword", 0.05),
                    new ItemLoot("Wand of Ancient Terror", 0.05),
                    new ItemLoot("Bow of Nightmares", 0.05)
                ),
                new Threshold(0.1,
                    new ItemLoot("Candy Corn", 0.1)
                )
      )

        .Init("Zombie Paladin",
            new State(
                    new StayCloseToSpawn(1, 4),
                    new Wander(0.4),
                new ConditionalEffect(ConditionEffectIndex.Armored, true),
                new State("RNG",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, false),
                    new TimedTransition(100, "Gx", true),
                    new TimedTransition(100, "G", true)
                    ),
                new State("Gx",
                    new Taunt(.1, 15000,
                        "GGHHhh... Rghh!",
                        "ARRghhhh",
                        "Rwarrhg!!"
                        ),
                    new Shoot(5, 1, coolDown: 1800)
                    ),
                new State("G",
                    new Shoot(5, 1, coolDown: 2000)
                 )
                    ),
                new MostDamagers(1,
                    new ItemLoot("Infected Skin", 0.001)
                ),
                new OnlyOne(
                    new ItemLoot("Corrupted Cleaver", 0.05),
                    new ItemLoot("Staff of Horrific Knowledge", 0.05),
                    new ItemLoot("Dagger of the Terrible Talon", 0.05),
                    new ItemLoot("Skull-splitter Sword", 0.05),
                    new ItemLoot("Wand of Ancient Terror", 0.05),
                    new ItemLoot("Bow of Nightmares", 0.05)
                ),
                new Threshold(0.1,
                    new ItemLoot("Candy Corn", 0.1)
                )
      )

        .Init("Zombie Necromancer",
            new State(
                    new StayCloseToSpawn(1, 4),
                    new Wander(0.4),
                new ConditionalEffect(ConditionEffectIndex.Armored, true),
                new State("RNG",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, false),
                    new TimedTransition(100, "Gx", true),
                    new TimedTransition(100, "G", true)
                    ),
                new State("Gx",
                    new Taunt(.1, 15000,
                        "GGHHhh... Rghh!",
                        "ARRghhhh",
                        "Rwarrhg!!"
                        ),
                    new Shoot(8, 2, 5, coolDown: 1800)
                    ),
                new State("G",
                    new Shoot(8, 2, 5, coolDown: 2000)
                 )
                    ),
                new MostDamagers(1,
                    new ItemLoot("Infected Skin", 0.001)
                ),
                new OnlyOne(
                    new ItemLoot("Corrupted Cleaver", 0.05),
                    new ItemLoot("Staff of Horrific Knowledge", 0.05),
                    new ItemLoot("Dagger of the Terrible Talon", 0.05),
                    new ItemLoot("Skull-splitter Sword", 0.05),
                    new ItemLoot("Wand of Ancient Terror", 0.05),
                    new ItemLoot("Bow of Nightmares", 0.05)
                ),
                new Threshold(0.1,
                    new ItemLoot("Candy Corn", 0.1)
                )
      )

        .Init("Zombie Huntress",
            new State(
                    new StayCloseToSpawn(1, 4),
                    new Wander(0.4),
                new ConditionalEffect(ConditionEffectIndex.Armored, true),
                new State("RNG",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, false),
                    new TimedTransition(100, "Gx", true),
                    new TimedTransition(100, "G", true)
                    ),
                new State("Gx",
                    new Taunt(.1, 15000,
                        "GGHHhh... Rghh!",
                        "ARRghhhh",
                        "Rwarrhg!!"
                        ),
                    new Shoot(7, 1, coolDown: 1800)
                    ),
                new State("G",
                    new Shoot(7, 1, coolDown: 2000)
                 )
                    ),
                new MostDamagers(1,
                    new ItemLoot("Infected Skin", 0.001)
                ),
                new OnlyOne(
                    new ItemLoot("Corrupted Cleaver", 0.05),
                    new ItemLoot("Staff of Horrific Knowledge", 0.05),
                    new ItemLoot("Dagger of the Terrible Talon", 0.05),
                    new ItemLoot("Skull-splitter Sword", 0.05),
                    new ItemLoot("Wand of Ancient Terror", 0.05),
                    new ItemLoot("Bow of Nightmares", 0.05)
                ),
                new Threshold(0.1,
                    new ItemLoot("Candy Corn", 0.1)
                )
      )

        .Init("Zombie Mystic",
            new State(
                    new StayCloseToSpawn(1, 4),
                    new Wander(0.4),
                new ConditionalEffect(ConditionEffectIndex.Armored, true),
                new State("RNG",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, false),
                    new TimedTransition(100, "Gx", true),
                    new TimedTransition(100, "G", true)
                    ),
                new State("Gx",
                    new Taunt(.1, 15000,
                        "GGHHhh... Rghh!",
                        "ARRghhhh",
                        "Rwarrhg!!"
                        ),
                    new Shoot(8, 2, 5, coolDown: 1800)
                    ),
                new State("G",
                    new Shoot(8, 2, 5, coolDown: 2000)
                 )
                    ),
                new MostDamagers(1,
                    new ItemLoot("Infected Skin", 0.001)
                ),
                new OnlyOne(
                    new ItemLoot("Corrupted Cleaver", 0.05),
                    new ItemLoot("Staff of Horrific Knowledge", 0.05),
                    new ItemLoot("Dagger of the Terrible Talon", 0.05),
                    new ItemLoot("Skull-splitter Sword", 0.05),
                    new ItemLoot("Wand of Ancient Terror", 0.05),
                    new ItemLoot("Bow of Nightmares", 0.05)
                ),
                new Threshold(0.1,
                    new ItemLoot("Candy Corn", 0.1)
                )
      )

        .Init("Zombie Trickster",
            new State(
                    new StayCloseToSpawn(1, 4),
                    new Wander(0.4),
                new ConditionalEffect(ConditionEffectIndex.Armored, true),
                new State("RNG",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, false),
                    new TimedTransition(100, "Gx", true),
                    new TimedTransition(100, "G", true)
                    ),
                new State("Gx",
                    new Taunt(.1, 15000,
                        "GGHHhh... Rghh!",
                        "ARRghhhh",
                        "Rwarrhg!!"
                        ),
                    new Shoot(6, 1, coolDown: 1800)
                    ),
                new State("G",
                    new Shoot(6, 1, coolDown: 2000)
                 )
                    ),
                new MostDamagers(1,
                    new ItemLoot("Infected Skin", 0.001)
                ),
                new OnlyOne(
                    new ItemLoot("Corrupted Cleaver", 0.05),
                    new ItemLoot("Staff of Horrific Knowledge", 0.05),
                    new ItemLoot("Dagger of the Terrible Talon", 0.05),
                    new ItemLoot("Skull-splitter Sword", 0.05),
                    new ItemLoot("Wand of Ancient Terror", 0.05),
                    new ItemLoot("Bow of Nightmares", 0.05)
                ),
                new Threshold(0.1,
                    new ItemLoot("Candy Corn", 0.1)
                )
      )

        .Init("Zombie Sorcerer",
            new State(
                    new StayCloseToSpawn(1, 4),
                    new Wander(0.4),
                new ConditionalEffect(ConditionEffectIndex.Armored, true),
                new State("RNG",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, false),
                    new TimedTransition(100, "Gx", true),
                    new TimedTransition(100, "G", true)
                    ),
                new State("Gx",
                    new Taunt(.1, 15000,
                        "GGHHhh... Rghh!",
                        "ARRghhhh",
                        "Rwarrhg!!"
                        ),
                    new Shoot(10, 1, coolDown: 1800)
                    ),
                new State("G",
                    new Shoot(10, 1, coolDown: 2000)
                 )
                    ),
                new MostDamagers(1,
                    new ItemLoot("Infected Skin", 0.001)
                ),
                new OnlyOne(
                    new ItemLoot("Corrupted Cleaver", 0.05),
                    new ItemLoot("Staff of Horrific Knowledge", 0.05),
                    new ItemLoot("Dagger of the Terrible Talon", 0.05),
                    new ItemLoot("Skull-splitter Sword", 0.05),
                    new ItemLoot("Wand of Ancient Terror", 0.05),
                    new ItemLoot("Bow of Nightmares", 0.05)
                ),
                new Threshold(0.1,
                    new ItemLoot("Candy Corn", 0.1)
                )
      )

        .Init("Zombie Assassin",
            new State(
                    new StayCloseToSpawn(1, 4),
                    new Wander(0.4),
                new ConditionalEffect(ConditionEffectIndex.Armored, true),
                new State("RNG",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, false),
                    new TimedTransition(100, "Gx-OC", true),
                    new TimedTransition(100, "G", true)
                    ),
                new State("Gx-OC",
                    new Taunt(.1, 15000,
                        "GGHHhh... Rghh!",
                        "ARRghhhh",
                        "Rwarrhg!!"
                        ),
                    new Grenade(3.5, 90, 7, coolDown: 4000),
                    new Shoot(6, 1, coolDown: 1800)
                    ),
                new State("G",
                    new Grenade(3.5, 90, 7, coolDown: 4500),
                    new Shoot(6, 1, coolDown: 2000)
                 )
                    ),
                new MostDamagers(1,
                    new ItemLoot("Infected Skin", 0.001)
                ),
                new OnlyOne(
                    new ItemLoot("Corrupted Cleaver", 0.05),
                    new ItemLoot("Staff of Horrific Knowledge", 0.05),
                    new ItemLoot("Dagger of the Terrible Talon", 0.05),
                    new ItemLoot("Skull-splitter Sword", 0.05),
                    new ItemLoot("Wand of Ancient Terror", 0.05),
                    new ItemLoot("Bow of Nightmares", 0.05)
                ),
                new Threshold(0.1,
                    new ItemLoot("Candy Corn", 0.1)
                )
      )

        .Init("Zombie Ninja",
            new State(
                    new StayCloseToSpawn(1, 4),
                    new Wander(0.4),
                new ConditionalEffect(ConditionEffectIndex.Armored, true),
                new State("RNG",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, false),
                    new TimedTransition(100, "Gx", true),
                    new TimedTransition(100, "G", true)
                    ),
                new State("Gx",
                    new Taunt(.1, 15000,
                        "GGHHhh... Rghh!",
                        "ARRghhhh",
                        "Rwarrhg!!"
                        ),
                    new Shoot(7, 1, coolDown: 1800)
                    ),
                new State("G",
                    new Shoot(7, 1, coolDown: 2000)
                 )
                    ),
                new MostDamagers(1,
                    new ItemLoot("Infected Skin", 0.001)
                ),
                new OnlyOne(
                    new ItemLoot("Corrupted Cleaver", 0.05),
                    new ItemLoot("Staff of Horrific Knowledge", 0.05),
                    new ItemLoot("Dagger of the Terrible Talon", 0.05),
                    new ItemLoot("Skull-splitter Sword", 0.05),
                    new ItemLoot("Wand of Ancient Terror", 0.05),
                    new ItemLoot("Bow of Nightmares", 0.05)
                ),
                new Threshold(0.1,
                    new ItemLoot("Candy Corn", 0.1)
                )
      )


        #region Final Brackets
            ;
    }
}
#endregion