﻿#region

using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;

#endregion

namespace wServer.logic
{
    partial class BehaviorDb
    {
        private _ Manor = () => Behav()

            /*
               .Init("Event Chest",
                new State(
                    new State("HW2_rvn",
                        new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                        new Taunt(true, "The Event Chest has spawned! It will become vulnerable in 10 seconds."),
                        new TimedTransition(10000, "HW2_rvn2")
                        ),
                    new State("HW2_rvn2",
                        new ConditionalEffect(ConditionEffectIndex.ArmorBroken)
                        )
                    ),
                new OnlyOne(
                    new ItemLoot("event card hw1", 1),
                    new ItemLoot("event card hw2", 1),
                    new ItemLoot("event card hw3", 1),
                    new ItemLoot("event card hw4", 0.12)
                    ),
                new MostDamagers(3,
                    new ItemLoot("Corrupted Cleaver", 0.1),
                    new ItemLoot("Staff of Horrific Knowledge", 0.1),
                    new ItemLoot("Dagger of the Terrible Talon", 0.1),
                    new ItemLoot("Skull-splitter Sword", 0.1),
                    new ItemLoot("Wand of Ancient Terror", 0.1),
                    new ItemLoot("Bow of Nightmares", 0.1),
                    new ItemLoot("Infected Skin", 0.007)
                    )
                )
            */

                    .Init("Lord Ruthven", //12k HP
                new State(
                    new HpLessTransition(3000, "rageven"),
                    //new TransformOnDeath("Event Chest", 1, 1, 1), //oct.29 ~ nov.8th
                    new RealmPortalDrop(),
                    new State("default",
                        new PlayerWithinTransition(8, "spooksters")
                        ),
                    new State("spookstersPrep",
                        new ReturnToSpawn(),
                        new TimedTransition(900, "spooksters"),
                        new ConditionalEffect(ConditionEffectIndex.Invulnerable)
                        ),
                    new State("spooksters",
                        new Wander(0.2),
                        new SetAltTexture(0),
                        new StayCloseToSpawn(0.15, 5),
                        new Shoot(10, count: 5, shootAngle: 4.3, projectileIndex: 0, coolDown: 900),
                        new TimedTransition(6000, "spooksters2")
                        ),
                    new State("spooksters2",
                        new Wander(0.15),
                        new StayCloseToSpawn(0.15, 5),
                        new Shoot(8.4, count: 40, projectileIndex: 1, coolDown: 2750),
                        new Shoot(10, count: 5, shootAngle: 2, projectileIndex: 0, coolDown: 900),
                        new TimedTransition(4000, "ruthven_batsPrep")
                        ),
                    new State("ruthven_batsPrep",
                        new ReturnToSpawn(true, 1.5),
                        new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                        new SpecificHeal(2, 200, "Self", 700),
                        new TossObject("Coffin Creature", 6.5, 0, 4200),
                        new TossObject("Coffin Creature", 6.5, 90, 4200),
                        new TossObject("Coffin Creature", 6.5, 180, 4200),
                        new TossObject("Coffin Creature", 6.5, 270, 4200),
                        new TimedTransition(1000, "ruthven_bats")
                        ),
                    new State("ruthven_bats",
                        new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                        new SpecificHeal(2, 200, "Self", 700),
                        new Shoot(10, count: 18, projectileIndex: 1, coolDown: 1800, coolDownOffset: 450),
                        new Shoot(10, count: 18, projectileIndex: 1, coolDown: 1800, coolDownOffset: 900),
                        new Shoot(10, count: 18, projectileIndex: 1, coolDown: 1800, coolDownOffset: 1350),
                        new Shoot(10, count: 18, projectileIndex: 1, coolDown: 1800, coolDownOffset: 1800),
                        new TimedTransition(2000, "ruthven_bats2")
                        ),
                    new State("ruthven_bats2",
                        new SetAltTexture(1),
                        new StayCloseToSpawn(0.5, 10),
                        new SpecificHeal(2, 100, "Self", 800),
                        new Follow(0.5, 12, 0),
                        new Reproduce("Vampire Bat", 10, densityMax: 25, spawnRadius: 1, coolDown: 200),
                        new TimedTransition(20000, "spookstersPrep"),
                        new EntityNotExistsTransition("Coffin Creature", 25, "spookstersPrep")
                        ),
                    new State("rageven",
                        new SetAltTexture(2),
                        new Flash(0xFF0000, 9999999, 1),
                        new ConditionalEffect(ConditionEffectIndex.Armored),
                        new Shoot(10, count: 20, projectileIndex: 1, coolDown: 1800, coolDownOffset: 450),
                        new Shoot(10, count: 20, projectileIndex: 1, coolDown: 1800, coolDownOffset: 900),
                        new Shoot(10, count: 20, projectileIndex: 1, coolDown: 1800, coolDownOffset: 1350),
                        new Shoot(10, count: 20, projectileIndex: 1, coolDown: 1800, coolDownOffset: 1800)
                        

                        )
                    ),
                 new MostDamagers(5,
                    new ItemLoot("Holy Water", 1),
                     new ItemLoot("Potion of Attack", 1),
                     new ItemLoot("Wine Cellar Incantation", 0.01),
                     new ItemLoot("Bone Dagger", 0.04)
                            ),
                 new MostDamagers(2,
                    new ItemLoot("Chasuble of Holy Light", 0.08),
                    new ItemLoot("St. Abraham's Wand", 0.08),
                    new ItemLoot("Tome of Purification", 0.02),
                    new ItemLoot("Ring of Divine Faith", 0.08)
                            ),
                new Threshold(0.025, //max 2-3 items maybe variable?
                    new TierLoot(9, ItemType.Weapon, 0.4),
                    new TierLoot(9, ItemType.Armor, 0.4),
                    new TierLoot(3, ItemType.Ring, 0.3),
                    new TierLoot(4, ItemType.Ability, 0.2),
                    new TierLoot(10, ItemType.Armor, 0.2),
                    new TierLoot(10, ItemType.Weapon, 0.2),
                    new TierLoot(4, ItemType.Ring, 0.15)
                    )
            )
            .Init("Hellhound",
                new State(
                    new State("hellhound",
                    new TimedTransition(3000, "hellhound2"),
                    new Follow(1.25, 8, 1, coolDown: 440),
                    new Shoot(10, count: 5, shootAngle: 7, coolDown: 1600)
                        ),
                    new State("hellhound2",
                        new TimedTransition(3000, "hellhound"),
                    new Wander(0.3),
                    new Shoot(10, count: 5, shootAngle: 7, coolDown: 1600)
                        )
                    ),
                new ItemLoot("Health Potion", 0.05),
                new Threshold(0.5,
                    new ItemLoot("Timelock Orb", 0.01)
                    )
            )
            .Init("Vampire Bat",
                new State(
                    new Wander(0.4),
                    new Protect(1, "Lord Ruthven", 15, 2, 5),
                    new Shoot(10, count: 1, coolDown: 66)
                    )
            )

            .Init("Vampire Bat Swarmer",
                new State(
                    new State("kooolx",
                        new PlayerWithinTransition(8, "1")
                        ),
                    new State("1",
                    new Wander(0.1),
                    new Follow(1.5, 8, 1),
                    new Shoot(10, count: 1, coolDown: 66)
                    )
            )
            )
                    .Init("Lil Feratu",
                new State(
                    new Wander(0.1),
                    new Follow(0.35, 8, 1),
                    new Shoot(10, count: 6, shootAngle: 2, coolDown: 900)
                    ),
                new ItemLoot("Magic Potion", 0.05),
                new Threshold(0.5,
                    new ItemLoot("Steel Helm", 0.01)
                    )
            )
                            .Init("Lesser Bald Vampire",
                new State(
                    new Wander(0.1),
                    new Follow(0.35, 8, 1),
                    new Shoot(10, count: 5, shootAngle: 6, coolDown: 1000)
                    ),
                new ItemLoot("Magic Potion", 0.05),
                new Threshold(0.5,
                    new ItemLoot("Steel Helm", 0.01)
                    )
            )
                  .Init("Nosferatu",
                new State(
                    new Wander(0.25),
                    new Shoot(10, count: 5, shootAngle: 2, projectileIndex: 1, coolDown: 1000),
                    new Shoot(10, count: 6, shootAngle: 90, projectileIndex: 0, coolDown: 1500)
                    ),
                new ItemLoot("Magic Potion", 0.05),
                new Threshold(0.5,
                    new ItemLoot("Bone Dagger", 0.01),
                    new ItemLoot("Wand of Death", 0.05),
                    new ItemLoot("Golden Bow", 0.04),
                    new ItemLoot("Steel Helm", 0.05),
                    new ItemLoot("Ring of Paramount Defense", 0.09)
                    )
            )
            .Init("Armor Guard",
                new State(
                    new Wander(0.2),
                    new State("jek",
                        new PlayerWithinTransition(5, "jek2"),
                        new ConditionalEffect(ConditionEffectIndex.Invincible)
                        ),
                    new State("jek2",
                        new TossObject("RockBomb", 7, coolDown: 3000, randomToss: true),
                        new Shoot(10, count: 1, projectileIndex: 0, predictive: 7, coolDown: 1000),
                        new Shoot(10, count: 1, projectileIndex: 1, coolDown: 750)
                        )
                    ),
                new ItemLoot("Health Potion", 0.05),
                new Threshold(0.5,
                    new ItemLoot("Glass Sword", 0.01),
                    new ItemLoot("Staff of Destruction", 0.01),
                    new ItemLoot("Golden Shield", 0.01),
                    new ItemLoot("Ring of Paramount Speed", 0.01)
                    )
            )
            .Init("Coffin Creature",
                new State(
                    new Spawn("Lil Feratu", initialSpawn: 1, maxChildren: 2, coolDown: 2250),
                    new Shoot(10, count: 1, projectileIndex: 0, coolDown: 700)
                    ),
                new ItemLoot("Health Potion", 0.05)
            )

                    .Init("RockBomb",
                    new State(
                    new State("BOUTTOEXPLODE",
                        new TimedTransition(1111, "boom")
                        ),
                    new State("boom",
                        new Shoot(8.4, count: 1, fixedAngle: 0, projectileIndex: 0, coolDown: 1000),
                        new Shoot(8.4, count: 1, fixedAngle: 90, projectileIndex: 0, coolDown: 1000),
                        new Shoot(8.4, count: 1, fixedAngle: 180, projectileIndex: 0, coolDown: 1000),
                        new Shoot(8.4, count: 1, fixedAngle: 270, projectileIndex: 0, coolDown: 1000),
                        new Shoot(8.4, count: 1, fixedAngle: 45, projectileIndex: 0, coolDown: 1000),
                        new Shoot(8.4, count: 1, fixedAngle: 135, projectileIndex: 0, coolDown: 1000),
                        new Shoot(8.4, count: 1, fixedAngle: 235, projectileIndex: 0, coolDown: 1000),
                        new Shoot(8.4, count: 1, fixedAngle: 315, projectileIndex: 0, coolDown: 1000),
                       new Suicide()
                    )
            )
    )
           .Init("Coffin",
                        new State(
                    new State("Coffin1",
                        new HpLessTransition(0.75, "Coffin2")
                        ),
                    new State("Coffin2",
                        new Spawn("Vampire Bat Swarmer", initialSpawn: 1, maxChildren: 15, coolDown: 99999),
                         new HpLessTransition(0.40, "Coffin3")
                        ),
                       new State("Coffin3",
                           new Spawn("Vampire Bat Swarmer", initialSpawn: 1, maxChildren: 8, coolDown: 99999),
                            new Spawn("Nosferatu", initialSpawn: 1, maxChildren: 2, coolDown: 99999)
                        )
                ),
                 new MostDamagers(1,
                     new ItemLoot("Chasuble of Holy Light", 0.01),
                     new ItemLoot("St. Abraham's Wand", 0.01),
                     new ItemLoot("Tome of Purification", 0.001),
                     new ItemLoot("Ring of Divine Faith", 0.01)
                            ),
                 new MostDamagers(5,
                    new ItemLoot("Holy Water", 1.00),
                     new ItemLoot("Potion of Attack", 0.5),
                     new ItemLoot("Wine Cellar Incantation", 0.01),
                     new ItemLoot("Bone Dagger", 0.03)
                            ),
                new Threshold(0.5,
                     new TierLoot(7, ItemType.Weapon, 0.05),
                     new TierLoot(6, ItemType.Armor, 0.2),
                     new TierLoot(4, ItemType.Ability, 0.15)
                    )
            )
        ;
    }
}