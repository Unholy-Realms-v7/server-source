﻿#region

using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;

#endregion

namespace wServer.logic
{
    partial class BehaviorDb
    {
        private _ ForbiddenJungle = () => Behav()
            .Init("Great Coil Snake",
                new State(
                    new DropPortalOnDeath("Forbidden Jungle Portal", 65),
                    new Prioritize(
                        new StayCloseToSpawn(0.8, 5),
                        new Wander(0.4)
                        ),
                    new State("Waiting",
                        new PlayerWithinTransition(15, "Attacking")
                        ),
                    new State("Attacking",
                        new Shoot(10, projectileIndex: 0, coolDown: 700, coolDownOffset: 600),
                        new Shoot(10, 10, 36, 1, coolDown: 2000),
                        new TossObject("Great Snake Egg", 4, 0, 5000, coolDownOffset: 0),
                        new TossObject("Great Snake Egg", 4, 90, 5000, 600),
                        new TossObject("Great Snake Egg", 4, 180, 5000, 1200),
                        new TossObject("Great Snake Egg", 4, 270, 5000, 1800),
                        new NoPlayerWithinTransition(30, "Waiting")
                        )
                    )
            )


            .Init("Mixcoatl the Masked God",
                new State(
                    new DamageTakenTransition(3000, "WeaponsB1"),
                    new DamageTakenTransition(4050, "Die"),
                    new State("Wait",
                        new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                        new PlayerWithinTransition(10, "Weapons1")
                       ),
                    new State("Weapons1",
                        new Wander(0.1),
                        new HpLessTransition(0.7, "Weapons3"),
                        new TimedTransition(3500, "Weapons2"),
                        new Shoot(10, 5, projectileIndex: 0, angleOffset: 10, coolDown: 2000, coolDownOffset: 1400),
                        new Shoot(10, 5, projectileIndex: 0, fixedAngle: 25, angleOffset: 10, coolDown: 2000, coolDownOffset: 1900)
                        ),
                    new State("Weapons2",
                        new Wander(0.1),
                        new HpLessTransition(0.7, "Weapons3"),
                        new TimedTransition(3500, "Weapons1"),
                        new Shoot(10, 1, projectileIndex: 2, coolDown: 900)
                    ),
                    new State("Weapons3",
                        new Wander(0.1),
                        new Shoot(10, 5, shootAngle: 10, projectileIndex: 1, coolDown: 2500, coolDownOffset: 1500),
                        new Shoot(10, 5, shootAngle: 10, projectileIndex: 1, coolDown: 2500, coolDownOffset: 1700),
                        new Shoot(10, 5, shootAngle: 10, projectileIndex: 1, coolDown: 2500, coolDownOffset: 1900),
                        new Shoot(10, 3, shootAngle: 10, projectileIndex: 1, coolDown: 2500, coolDownOffset: 2100)
                        ),
                    new State("WeaponsB1",
                        new TimedTransition(200, "WeaponsB2"),
                        new Order(10, "Boss Totem", "WeAre"),
                        new StayCloseToSpawn(0.4, 1)
                        ),
                    new State("WeaponsB2",
                        new TimedTransition(2600, "Sync"),
                        new StayCloseToSpawn(0.4, 5),
                        new Shoot(10, 5, shootAngle: 10, projectileIndex: 1, coolDown: 2500, coolDownOffset: 1700),
                        new Shoot(10, 5, shootAngle: 10, projectileIndex: 1, coolDown: 2500, coolDownOffset: 1800),
                        new Shoot(10, 5, shootAngle: 10, projectileIndex: 1, coolDown: 2500, coolDownOffset: 1900),
                        new Shoot(10, 3, shootAngle: 10, projectileIndex: 1, coolDown: 2500, coolDownOffset: 2000),
                        new Shoot(10, 1, projectileIndex: 2, coolDown: 900)
                        ),
                    new State("Sync",
                        new TimedTransition(100, "WeaponsB2")
                        ),
                    new State("Die",
                        new Order(10, "Boss Totem", "Wait"),
                        new Shoot(15, 9, projectileIndex: 1),
                        new Suicide()
                        )
                    ),
                new Threshold(0.025,
                    new TierLoot(5, ItemType.Weapon, 0.24),
                    new TierLoot(6, ItemType.Weapon, 0.12),
                    new TierLoot(6, ItemType.Armor, 0.24),
                    new ItemLoot("Pollen Powder", 1)
                    ),
                new Threshold(0.01,
                    new ItemLoot("Staff of the Crystal Serpent", 0.05),
                    new ItemLoot("Cracked Crystal Skull", 0.05),
                    new ItemLoot("Robe of the Tlatoani", 0.05),
                    new ItemLoot("Crystal Bone Ring", 0.05)
                    )

            )


            .Init("Boss Totem",
                new State(
                        new ConditionalEffect(ConditionEffectIndex.Invincible, true),
                        new EntityNotExistsTransition("Mixcoatl the Masked God", 15, "Wait"),
                    new State("Wait"
                        ),
                    new State("WeAre",
                        new Shoot(10, 10, fixedAngle: 10, coolDown: 3000, coolDownOffset: 1500)
                        )
                    )
            )


            .Init("Great Snake Egg",
                new State(
                    new TransformOnDeath("Great Temple Snake", 1, 2),
                    new State("Wait",
                        new TimedTransition(2500, "Explode"),
                        new PlayerWithinTransition(2, "Explode")
                        ),
                    new State("Explode",
                        new Suicide()
                        )
                    )
            )
            .Init("Great Temple Snake",
                new State(
                    new Prioritize(
                        new Follow(0.6),
                        new Wander(0.4)
                        ),
                    new Shoot(10, 2, 7, 0, coolDown: 1000, coolDownOffset: 0),
                    new Shoot(10, 6, 60, 1, coolDown: 2000, coolDownOffset: 600)
                    )
            )
            ;
    }
}