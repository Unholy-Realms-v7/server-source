﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using wServer.realm;
using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;

namespace wServer.logic
{
    partial class BehaviorDb
    {
        _ CubeGod = () => Behav()
            .Init("Cube God",
                new State(
                    new State("1",
                    new StayCloseToSpawn(0.3, range: 7),
                           new Wander(0.5),
                           new HpLessTransition(0.5, "2"),
                             new Shoot(10, count: 9, predictive: 0.9, shootAngle: 6.5, coolDown: 1000),
                             new Spawn("Cube Overseer", maxChildren: 5, initialSpawn: 3, coolDown: 100000),
                             new Spawn("Cube Defender", maxChildren: 5, initialSpawn: 5, coolDown: 100000),
                             new Spawn("Cube Blaster", maxChildren: 5, initialSpawn: 5, coolDown: 100000)
                        ),
                    new State("2",
                        new StayCloseToSpawn(0.6, range: 7),
                        new Wander(0.5),
                        new Shoot(10, count: 11, predictive: 0.9, shootAngle: 6.5, coolDown: 900),
                        new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                        new TimedTransition(3000, "3"),
                        new Spawn("Cube Overseer", maxChildren: 5, initialSpawn: 3, coolDown: 100000),
                        new Spawn("Cube Defender", maxChildren: 5, initialSpawn: 5, coolDown: 100000),
                        new Spawn("Cube Blaster", maxChildren: 5, initialSpawn: 5, coolDown: 100000),
                        new Flash(0x0006cc, 0.5, 9999999)
                        ),
                    new State("3",
                        new HpLessTransition(0.22, "4"),
                        new ConditionalEffect(ConditionEffectIndex.Armored, true),
                        new StayCloseToSpawn(0.6, range: 7),
                        new Wander(0.6),
                        new Shoot(10, count: 11, predictive: 0.9, shootAngle: 6.5, coolDown: 900),
                        new Flash(0x0006cc, 1.2, 9999999)
                        ),
                    new State("4",
                        new StayCloseToSpawn(0.75, range: 7),
                        new Wander(0.75),
                        new Shoot(10, count: 15, predictive: 0.9, shootAngle: 6.5, coolDown: 710),
                        new Flash(0xFF0000, 1.4, 9999999)
                        )
                ),
                new Threshold(0.4,
                    new TierLoot(3, ItemType.Ring, 0.66),
                    new TierLoot(7, ItemType.Armor, 0.66),
                    new TierLoot(8, ItemType.Weapon, 0.66),
                    new TierLoot(4, ItemType.Ability, 0.66),
                    new TierLoot(8, ItemType.Armor, 0.66),
                    new TierLoot(4, ItemType.Ring, 0.66),
                    new TierLoot(9, ItemType.Armor, 0.66),
                    new TierLoot(5, ItemType.Ability, 0.08),
                    new TierLoot(9, ItemType.Weapon, 0.66),
                    new TierLoot(10, ItemType.Armor, 0.66),
                    new TierLoot(10, ItemType.Weapon, 0.55),
                    new TierLoot(11, ItemType.Armor, 0.33),
                    new TierLoot(11, ItemType.Weapon, 0.16),
                    new TierLoot(5, ItemType.Ring, 0.08),
                    new ItemLoot("Dirk of Cronus", 0.07),
                    new ItemLoot("Potion of Dexterity", 1),
                    new ItemLoot("Potion of Attack", 1)
                )
            )
            .Init("Cube Overseer",
                new State(
                    new StayCloseToSpawn(0.3, range: 7),
                             new Wander(1),
                             new Shoot(10, count: 4, predictive: 0.9, projectileIndex: 0, coolDown: 1250)
                )
            )
            .Init("Cube Defender",
                new State(
                    new Wander(0.5),
                             new StayCloseToSpawn(0.03, range: 7),
                             new Follow(0.4, acquireRange: 9, range: 2),
                             new Shoot(10, count: 1, coolDown: 1000, predictive: 0.9, projectileIndex: 0)
                )
            )
            .Init("Cube Blaster",
                new State(
                    new Wander(0.5),
                             new StayCloseToSpawn(0.03, range: 7),
                             new Follow(0.4, acquireRange: 9, range: 2),
                             new Shoot(10, count: 2, predictive: 0.9, projectileIndex: 0, coolDown: 1500),
                             new Shoot(10, count: 1, predictive: 0.9, projectileIndex: 0, coolDown: 1500)
                )
            );
    }
}
