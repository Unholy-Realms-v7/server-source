﻿using wServer.logic.behaviors;
using wServer.logic.transitions;
using wServer.logic.loot;

namespace wServer.logic
{
    partial class BehaviorDb
    {
        private _ DeadwaterDocks = () => Behav()
              .Init("Deadwater Docks Parrot",
                  new State(
                    new ChangeSize(100, 90),
                    new EntityNotExistsTransition("Jon Bilgewater the Pirate King", 90000, "rip"),
                    new State("CircleOrWander",
                        new Prioritize(
                            new Orbit(1.05, 1.25, 5, "Parrot Cage"),
                            new Wander(0.12)
                            )
                        ),
                    new State("Orbit&HealJon",
                        new Shoot(6, 1, null, 0, null, 0, null, 0.5, 0, coolDown:1700),
                    new Orbit(1.05, 1.25, 20, "Jon Bilgewater the Pirate King"),
                    new SpecificHeal(2, 200, "JonBilgewater", coolDown: 2500)
                    ),
                    new State("Orbit&HealJonR",
                        new ChangeSize(100, 115),
                        new Shoot(6, 1, null, 0, null, 0, null, 0.5, 0, coolDown: 1000),
                    new Orbit(1.05, 1.25, 20, "Jon Bilgewater the Pirate King"),
                    new SpecificHeal(2, 200, "JonBilgewater", coolDown: 2000)
                    ),
                    new State("rip",
                    new Suicide()
                    )
                 )
              )
              .Init("Parrot Cage",
                  new State(
                    new ConditionalEffect(ConditionEffectIndex.Invincible, true),
                    new State("NoSpawn"
                        ),
                    new State("SpawnParrots",
                    new Reproduce("Deadwater Docks Parrot", 5, densityMax: 5, coolDown: 700)
                        ),
                    new State("SpawnParrotsOVERDRIVE",
                    new Reproduce("Deadwater Docks Parrot", 5, densityMax: 5, spawnRadius: 5, coolDown: 2500)
                    )
                 )
              )
             .Init("Bottled Evil Water",
                 new State(
                    new State("water",
                    new ConditionalEffect(ConditionEffectIndex.Invincible),
                    new TimedTransition(1500, "drop")
                        ),
                    new State("drop",
                       new ApplySetpiece("BottledEvil"),
                       new Suicide()
                    )))
          .Init("Deadwater Docks Lieutenant",
                new State(
                    new Wander(0.3),
                    new Follow(1.1, 8, 1),
                    new Shoot(8, 1, 10, coolDown: 1000),
                    new TossObject("Bottled Evil Water", angle: null, coolDown: 6750)
                    ),
                new ItemLoot("Health Potion", 0.1),
                new ItemLoot("Magic Potion", 0.1)
            )
          .Init("Deadwater Docks Veteran",
                new State(
                    new Wander(0.3),
                    new Follow(1.1, 8, 1),
                    new Shoot(8, 1, 10, coolDown: 500)
                    ),
                new TierLoot(10, ItemType.Weapon, 0.0065),
                new ItemLoot("Health Potion", 0.1),
                new ItemLoot("Magic Potion", 0.1)
            )
          .Init("Deadwater Docks Admiral",
                new State(
                    new Wander(0.3),
                    new Follow(1.1, 8, 1),
                    new Shoot(8, 3, 10, coolDown: 1325)
                    ),
                new ItemLoot("Health Potion", 0.1),
                new ItemLoot("Magic Potion", 0.1)
            )
          .Init("Deadwater Docks Brawler",
                new State(
                    new Wander(0.3),
                    new Charge(2, 10, 4000),
                    new Follow(1.22, 8, 1),
                    new Shoot(8, 1, 10, coolDown: 350)
                    ),
                new ItemLoot("Health Potion", 0.1),
                new ItemLoot("Magic Potion", 0.1)
            )
          .Init("Deadwater Docks Sailor",
                new State(
                    new Wander(0.3),
                    new Follow(1.1, 8, 1),
                    new Shoot(8, 1, 10, coolDown: 525)
                    ),
                new ItemLoot("Health Potion", 0.1),
                new ItemLoot("Magic Potion", 0.1)
            )
          .Init("Deadwater Docks Commander",
                new State(
                    new Wander(0.3),
                    new Follow(1.1, 8, 1),
                    new Shoot(8, 1, 10, coolDown: 900),
                    new TossObject("Bottled Evil Water", angle: null, coolDown: 8750)
                    ),
                new ItemLoot("Health Potion", 0.1),
                new ItemLoot("Magic Potion", 0.1)
            )
          .Init("Deadwater Docks Captain",
                new State(
                    new Wander(0.3),
                    new Follow(1.1, 8, 1),
                    new Shoot(8, 1, 10, coolDown: 3500)
                    ),
                new ItemLoot("Health Potion", 0.1),
                new ItemLoot("Magic Potion", 0.1)
            )

          .Init("Jon Bilgewater the Pirate King",
                new State(
                    new HpLessTransition(0.3, "rage"),
                    new StayCloseToSpawn(0.5, 3),
                    new OnDeathBehavior(new Order(100, "Parrot Cage", "NoSpawn")),
                    new RealmPortalDrop(),
                    new State("default",
                        new PlayerWithinTransition(8, "coinphase")
                        ),
                  new State(
                      new Order(90, "Parrot Cage", "SpawnParrots"),
                    new DamageTakenTransition(32500, "gotoSpawn"),
                    new State("coinphase",
                        new Wander(0.11),
                        new Shoot(10, count: 1, projectileIndex: 0, coolDown: 2000),
                        new TimedTransition(4500, "cannonballs")
                        ),
                    new State("cannonballs",
                        new Follow(0.32, 8, coolDown: 1000),
                        new Shoot(10, count: 7, shootAngle: 30, projectileIndex: 1, coolDown: 2150),
                        new TimedTransition(5000, "coinphase")
                        )
                        ),

                    new State("gotoSpawn",
                        new Order(100, "Parrot Cage", "SpawnParrotsOVERDRIVE"),
                        new ReturnToSpawn(once: true, speed: 0.52),
                        new ConditionalEffect(ConditionEffectIndex.Invincible),
                        new ConditionalEffect(ConditionEffectIndex.StunImmune),
                        new TimedTransition(3500, "blastcannonballs")
                        ),
                    new State("blastcannonballs",
                        new Order(100, "Parrot Cage", "NoSpawn"),
                        new ConditionalEffect(ConditionEffectIndex.StunImmune),
                        new Order(90, "Deadwater Docks Parrot", "CircleOrWander"),
                        new Shoot(10, count: 7, shootAngle: 30, projectileIndex: 1, coolDown: 1750),
                        new TimedTransition(6000, "parrotcircle")
                        ),
                    new State("parrotcircle",
                        new ConditionalEffect(ConditionEffectIndex.StunImmune),
                        new Order(90, "Deadwater Docks Parrot", "Orbit&HealJon"),
                        new EntityNotExistsTransition("Deadwater Docks Parrot", 100, "gotoSpawn"),
                        new TimedTransition(6000, "blastcannonballs")
                        ),
                    new State("rage",
                        new Taunt(true, "ARGHHH!"),
                        new ConditionalEffect(ConditionEffectIndex.Armored),
                        new ConditionalEffect(ConditionEffectIndex.StunImmune),
                        new Order(90, "Parrot Cage", "NoSpawn"),
                        new Order(90, "Deadwater Docks Parrot", "Orbit&HealJonR"),
                        new Shoot(10, count: 5, shootAngle: 25, projectileIndex: 1, coolDown: 1750),
                        new Shoot(10, count: 1, projectileIndex: 0, coolDown: 1750),
                        new TimedTransition(6000, "blastcannonballs")
                        )
                    ),
                new MostDamagers(3,
                    new ItemLoot("Potion of Speed", 1.0)
                ),
                new MostDamagers(1,
                    new ItemLoot("Potion of Dexterity", 1.0)
                ),
                new Threshold(0.025,
                    new TierLoot(11, ItemType.Weapon, 0.02),
                    new TierLoot(4, ItemType.Ability, 0.02),
                    new TierLoot(11, ItemType.Armor, 0.05),
                    new TierLoot(10, ItemType.Weapon, 0.05),
                    new TierLoot(4, ItemType.Ring, 0.025),
                    new ItemLoot("Pirate King's Cutlass", 0.07)
                )
            )

            ;
    }
}
