﻿
using wServer.logic.behaviors;
using wServer.logic.transitions;
using wServer.logic.loot;

namespace wServer.logic
{
    partial class BehaviorDb
    {
        private _ Nexus = () => Behav() //the best behavior yet, nexus is way cooler now.
            .Init("Winter Bunny",
                new State(
                    new StayCloseToSpawn(0.25, 4),
                    new StayBack(0.25, 4),
                    new ConditionalEffect(ConditionEffectIndex.Invincible),
                    new Wander(0.25)
                    ),
                new ItemLoot("Draconis Potion", 1.00)
                    )


            .Init("Butterfly",
                new State(
                    new StayCloseToSpawn(0.15, 4),
                    new StayBack(0.15, 4),
                    new ConditionalEffect(ConditionEffectIndex.Invincible),
                    new Wander(0.15)
                    ),
                new ItemLoot("Draconis Potion", 1.00)
                    )



            ;
    }
}