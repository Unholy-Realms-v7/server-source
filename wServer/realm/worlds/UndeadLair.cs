﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wServer.realm.worlds
{
    public class UndeadLair : World
    {
        public UndeadLair()
        {
            Name = "Undead Lair";
            ClientWorldName = "dungeons.Undead_Lair";
            Dungeon = true;
            Background = 0;
            AllowTeleport = true;
        }

        protected override void Init()
        {
            Random r = new Random();
            LoadMap("wServer.realm.worlds.maps.udl.UDL" + r.Next(1, 2 + 1).ToString() + ".jm", MapType.Json);
        }
    }
}