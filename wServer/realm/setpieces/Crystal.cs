﻿namespace wServer.realm.setpieces
{
    internal class Crystal : ISetPiece
    {
        public int Size
        {
            get { return 5; }
        }

        public void RenderSetPiece(World world, IntPoint pos)
        {
            Entity crystal = Entity.Resolve(world.Manager, "Mysterious Crystal");
            crystal.Move(pos.X + 2.5f, pos.Y + 2.5f);
            world.EnterWorld(crystal);
        }
    }
}