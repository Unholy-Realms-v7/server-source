﻿namespace wServer.realm.setpieces
{
    internal class Horsemen : ISetPiece
    {
        public int Size
        {
            get { return 5; }
        }

        public void RenderSetPiece(World world, IntPoint pos)
        {
            Entity horsemen = Entity.Resolve(world.Manager, "Arena Headless Horseman");
            horsemen.Move(pos.X + 2.5f, pos.Y + 2.5f);
            world.EnterWorld(horsemen);
        }
    }
}