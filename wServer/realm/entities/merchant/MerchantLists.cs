﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using db.data;
using log4net;

#endregion

namespace wServer.realm.entities
{
    internal class MerchantLists
    {
        public static int[] AccessoryClothList;
        public static int[] AccessoryDyeList;
        public static int[] ClothingClothList;
        public static int[] ClothingDyeList;

        public static Dictionary<int, Tuple<int, CurrencyType>> prices = new Dictionary<int, Tuple<int, CurrencyType>>
        {
            {0xb41, new Tuple<int, CurrencyType>(8220, CurrencyType.FortuneTokens)},
            {0xbab, new Tuple<int, CurrencyType>(8220, CurrencyType.FortuneTokens)},
            {0xbad, new Tuple<int, CurrencyType>(8220, CurrencyType.FortuneTokens)},

            //WEAPONS
            {0xa87, new Tuple<int, CurrencyType>(490, CurrencyType.Fame)},
            {0xa86, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xa85, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xa07, new Tuple<int, CurrencyType>(150, CurrencyType.Fame)},

            {0xa8d, new Tuple<int, CurrencyType>(490, CurrencyType.Fame)},
            {0xa8c, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xa8b, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xa1e, new Tuple<int, CurrencyType>(150, CurrencyType.Fame)},
            
            {0xaa2, new Tuple<int, CurrencyType>(490, CurrencyType.Fame)},
            {0xaa1, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xaa0, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xa9f, new Tuple<int, CurrencyType>(150, CurrencyType.Fame)},
            
            {0xa47, new Tuple<int, CurrencyType>(490, CurrencyType.Fame)},
            {0xa84, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xa83, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xa82, new Tuple<int, CurrencyType>(150, CurrencyType.Fame)},
            
            {0xa8a, new Tuple<int, CurrencyType>(490, CurrencyType.Fame)},
            {0xa89, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xa88, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xa19, new Tuple<int, CurrencyType>(150, CurrencyType.Fame)},
            
            {0xc4f, new Tuple<int, CurrencyType>(490, CurrencyType.Fame)},
            {0xc4e, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xc4d, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xc4c, new Tuple<int, CurrencyType>(150, CurrencyType.Fame)},

            //Rings
            {0xabf, new Tuple<int, CurrencyType>(210, CurrencyType.Fame)},
            {0xac0, new Tuple<int, CurrencyType>(330, CurrencyType.Fame)}, //def
            {0xac1, new Tuple<int, CurrencyType>(210, CurrencyType.Fame)},
            {0xac2, new Tuple<int, CurrencyType>(210, CurrencyType.Fame)},
            {0xac3, new Tuple<int, CurrencyType>(210, CurrencyType.Fame)},
            {0xac4, new Tuple<int, CurrencyType>(210, CurrencyType.Fame)},
            {0xac5, new Tuple<int, CurrencyType>(330, CurrencyType.Fame)}, //hp
            {0xac6, new Tuple<int, CurrencyType>(330, CurrencyType.Fame)}, //mp

            {0xac7, new Tuple<int, CurrencyType>(370, CurrencyType.Fame)},
            {0xac8, new Tuple<int, CurrencyType>(490, CurrencyType.Fame)}, //def
            {0xac9, new Tuple<int, CurrencyType>(370, CurrencyType.Fame)},
            {0xaca, new Tuple<int, CurrencyType>(370, CurrencyType.Fame)},
            {0xacb, new Tuple<int, CurrencyType>(370, CurrencyType.Fame)},
            {0xacc, new Tuple<int, CurrencyType>(370, CurrencyType.Fame)},
            {0xacd, new Tuple<int, CurrencyType>(490, CurrencyType.Fame)}, //hp
            {0xace, new Tuple<int, CurrencyType>(490, CurrencyType.Fame)}, //mp
            //ARMORS
            {0xa96, new Tuple<int, CurrencyType>(490, CurrencyType.Fame)}, //12
            {0xa93, new Tuple<int, CurrencyType>(490, CurrencyType.Fame)}, //12
            {0xa90, new Tuple<int, CurrencyType>(490, CurrencyType.Fame)}, //12
            {0xa95, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)}, //11
            {0xa60, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)}, //11
            {0xa92, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)}, //11
            {0xa94, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)}, //10
            {0xa91, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)}, //10
            {0xa8e, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)}, //10
            {0xa13, new Tuple<int, CurrencyType>(150, CurrencyType.Fame)}, //9
            {0xa8f, new Tuple<int, CurrencyType>(150, CurrencyType.Fame)}, //9
            {0xad3, new Tuple<int, CurrencyType>(150, CurrencyType.Fame)}, //9

            //ABILITIES
            {0xa5b, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xa0c, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xa30, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xa55, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xae1, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xa65, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xa6b, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xaa8, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xaaf, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xab6, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xa46, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xb20, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xb32, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            {0xc58, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)},
            
            {0xad6, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xa33, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xa54, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xa59, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xa64, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xa6a, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xaa7, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xaae, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xab5, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xa45, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xb1f, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xb31, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xc57, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},
            {0xa0b, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)},

            /*
            //PET FOOD
            {0xccc, new Tuple<int, CurrencyType>(5, CurrencyType.FortuneTokens)},
            {0xccb, new Tuple<int, CurrencyType>(2, CurrencyType.FortuneTokens)},
            {0xcca, new Tuple<int, CurrencyType>(3, CurrencyType.FortuneTokens)},
            {0xcc9, new Tuple<int, CurrencyType>(2, CurrencyType.FortuneTokens)},
            {0xcc8, new Tuple<int, CurrencyType>(3, CurrencyType.FortuneTokens)},
            {0xcc7, new Tuple<int, CurrencyType>(4, CurrencyType.FortuneTokens)},
            {0xcc6, new Tuple<int, CurrencyType>(2, CurrencyType.FortuneTokens)},
            {0xcc5, new Tuple<int, CurrencyType>(2, CurrencyType.FortuneTokens)},
            {0xcc4, new Tuple<int, CurrencyType>(2, CurrencyType.FortuneTokens)},

            //EGGS
            {0xc86, new Tuple<int, CurrencyType>(3, CurrencyType.FortuneTokens)}, //uncommon feline egg
            {0xc87, new Tuple<int, CurrencyType>(4, CurrencyType.FortuneTokens)}, //rare feline egg
            {0xc8a, new Tuple<int, CurrencyType>(3, CurrencyType.FortuneTokens)}, //uncommon canine egg
            {0xc8b, new Tuple<int, CurrencyType>(4, CurrencyType.FortuneTokens)}, //rare canine egg
            {0xc8e, new Tuple<int, CurrencyType>(3, CurrencyType.FortuneTokens)}, //uncommon avian egg
            {0xc8f, new Tuple<int, CurrencyType>(4, CurrencyType.FortuneTokens)}, //rare avian egg
            {0xc92, new Tuple<int, CurrencyType>(3, CurrencyType.FortuneTokens)}, //uncommon exotic egg
            {0xc93, new Tuple<int, CurrencyType>(4, CurrencyType.FortuneTokens)}, //rare exotic egg
            {0xc96, new Tuple<int, CurrencyType>(3, CurrencyType.FortuneTokens)}, //uncommon farm egg
            {0xc97, new Tuple<int, CurrencyType>(4, CurrencyType.FortuneTokens)}, //rare farm egg
            {0xc9a, new Tuple<int, CurrencyType>(3, CurrencyType.FortuneTokens)}, //uncommon woodland egg
            {0xc9b, new Tuple<int, CurrencyType>(4, CurrencyType.FortuneTokens)}, //rare woodland egg
            {0xc9e, new Tuple<int, CurrencyType>(3, CurrencyType.FortuneTokens)}, //uncommon reptile egg
            {0xc9f, new Tuple<int, CurrencyType>(4, CurrencyType.FortuneTokens)}, //rare reptile egg
            {0xca2, new Tuple<int, CurrencyType>(3, CurrencyType.FortuneTokens)}, //uncommon insect egg
            {0xca3, new Tuple<int, CurrencyType>(4, CurrencyType.FortuneTokens)}, //rare insect egg
            {0xca6, new Tuple<int, CurrencyType>(3, CurrencyType.FortuneTokens)}, //uncommon pinguin egg
            {0xca7, new Tuple<int, CurrencyType>(4, CurrencyType.FortuneTokens)}, //rare pinguin egg
            {0xcaa, new Tuple<int, CurrencyType>(3, CurrencyType.FortuneTokens)}, //uncommon aquatic egg
            {0xcab, new Tuple<int, CurrencyType>(4, CurrencyType.FortuneTokens)}, //rare aquatic egg
            {0xcae, new Tuple<int, CurrencyType>(3, CurrencyType.FortuneTokens)}, //uncommon spooky egg
            {0xcaf, new Tuple<int, CurrencyType>(4, CurrencyType.FortuneTokens)}, //rare spooky egg
            {0xcb2, new Tuple<int, CurrencyType>(3, CurrencyType.FortuneTokens)}, //uncommon humanoid egg
            {0xcb3, new Tuple<int, CurrencyType>(4, CurrencyType.FortuneTokens)}, //rare humanoid egg
            {0xcb6, new Tuple<int, CurrencyType>(3, CurrencyType.FortuneTokens)}, //uncommon ???? egg
            {0xcb7, new Tuple<int, CurrencyType>(4, CurrencyType.FortuneTokens)}, //rare ???? egg
            {0xcba, new Tuple<int, CurrencyType>(3, CurrencyType.FortuneTokens)}, //uncommon automaton egg
            {0xcbb, new Tuple<int, CurrencyType>(4, CurrencyType.FortuneTokens)}, //rare automaton egg
            {0xcbe, new Tuple<int, CurrencyType>(3, CurrencyType.FortuneTokens)}, //uncommon mystery egg
            {0xcbf, new Tuple<int, CurrencyType>(4, CurrencyType.FortuneTokens)}, //rare mystery egg
            */

            //Slime skins
            {0x226e, new Tuple<int, CurrencyType>(500, CurrencyType.Fame)},
            {0x227c, new Tuple<int, CurrencyType>(500, CurrencyType.Fame)},
            {0x227d, new Tuple<int, CurrencyType>(500, CurrencyType.Fame)},
            {0x258f, new Tuple<int, CurrencyType>(500, CurrencyType.Fame)},
            {0x2292, new Tuple<int, CurrencyType>(500, CurrencyType.Fame)},
            {0x2349, new Tuple<int, CurrencyType>(500, CurrencyType.Fame)},
            {0x234A, new Tuple<int, CurrencyType>(500, CurrencyType.Fame)},
            {0x234B, new Tuple<int, CurrencyType>(500, CurrencyType.Fame)},
            {0x234C, new Tuple<int, CurrencyType>(500, CurrencyType.Fame)},
            {0x234D, new Tuple<int, CurrencyType>(500, CurrencyType.Fame)},
            {0x234E, new Tuple<int, CurrencyType>(500, CurrencyType.Fame)},
            {0x234F, new Tuple<int, CurrencyType>(500, CurrencyType.Fame)},
            {0x2350, new Tuple<int, CurrencyType>(500, CurrencyType.Fame)},
            {0x2351, new Tuple<int, CurrencyType>(500, CurrencyType.Fame)},

            //Seasonal stuff  |  Bella
            {0x2290, new Tuple<int, CurrencyType>(80, CurrencyType.Fame)}, //Bella's Key - valentines <3
            {0x228a, new Tuple<int, CurrencyType>(50, CurrencyType.Fame)},
            {0x228b, new Tuple<int, CurrencyType>(50, CurrencyType.Fame)},
            {0x228c, new Tuple<int, CurrencyType>(50, CurrencyType.Fame)},
            {0x228d, new Tuple<int, CurrencyType>(50, CurrencyType.Fame)},
            {0x228e, new Tuple<int, CurrencyType>(50, CurrencyType.Fame)},
            {0x228f, new Tuple<int, CurrencyType>(50, CurrencyType.Fame)},
            {0xa4a, new Tuple<int, CurrencyType>(200, CurrencyType.Fame)},
            {0xa81, new Tuple<int, CurrencyType>(200, CurrencyType.Fame)},




            //Jakenbake, Paused, Stanley, I (in order)

//Undead lair key = 55, 145, 75, 130 = 101
//Pirate cave key = 31, 35, 15, 40 = 30
//Abyss of demons key = 55, 150, 75, 220 = 117
//Snake pit key = 55, 250, 75, 100 = 120
//Tomb of the ancients key = 170, 750, 150, 500 = 392
//Sprite World Key = 50, 50, 75, 100 = 68
//Ocean Trench Key = 140, 150, 150, 400 = 210
//Totem Key = 55, 50, 15, 80 = 50
//Manor Key = 75, 150, 150, 180 = 138
//Davy's Key = 80, 150, 150, 230 = 152
//Lab Key = 60, 150, 75, 180 = 116
//Deadwater docks key = 170, 450, 150, 270 = 260
//Woodland Labyrinth Key = 170, 350, 150, 320 = 247
//The crawling depths key = 170, 450, 150, 400 = 292
//Shatters key = 400, 950, 250, 550 = 537
//Draconis key = 390
//Ice Cave key = 210


            //KEYS
            {0x748C, new Tuple<int, CurrencyType>(210, CurrencyType.Fame)}, //Ice Cave key
            {0xcd4, new Tuple<int, CurrencyType>(390, CurrencyType.Fame)}, //Draconis key
            {0x701, new Tuple<int, CurrencyType>(100, CurrencyType.Fame)}, //Undead lair key
            {0x705, new Tuple<int, CurrencyType>(30, CurrencyType.Fame)}, //Pirate cave key
            {0x70a, new Tuple<int, CurrencyType>(115, CurrencyType.Fame)}, //Abyss of demons key
            {0x70b, new Tuple<int, CurrencyType>(120, CurrencyType.Fame)}, //Snake pit key
            {0x710, new Tuple<int, CurrencyType>(400, CurrencyType.Fame)}, //Tomb of the ancients key
            {0x71f, new Tuple<int, CurrencyType>(70, CurrencyType.Fame)}, //Sprite World Key
            {0xc11, new Tuple<int, CurrencyType>(210, CurrencyType.Fame)}, //Ocean Trench Key
            {0xc19, new Tuple<int, CurrencyType>(50, CurrencyType.Fame)}, //Totem Key
            {0xc23, new Tuple<int, CurrencyType>(140, CurrencyType.Fame)}, //Manor Key
            {0xc2e, new Tuple<int, CurrencyType>(160, CurrencyType.Fame)}, //Davy's Key
            {0xc2f, new Tuple<int, CurrencyType>(120, CurrencyType.Fame)}, //Lab Key
            {0xcce, new Tuple<int, CurrencyType>(260, CurrencyType.Fame)}, //Deadwater docks key
            {0xccf, new Tuple<int, CurrencyType>(250, CurrencyType.Fame)}, //Woodland Labyrinth Key
            {0xcda, new Tuple<int, CurrencyType>(300, CurrencyType.Fame)}, //The crawling depths key
            {0xcdd, new Tuple<int, CurrencyType>(540, CurrencyType.Fame)}, //Shatters key
        };

        public static int[] store10List = {0xb41, 0xbab, 0xbad, 0xbac};
        public static int[] store11List = {0xb41, 0xbab, 0xbad, 0xbac};
        public static int[] store12List = {0xb41, 0xbab, 0xbad, 0xbac};
        public static int[] store13List = {0xb41, 0xbab, 0xbad, 0xbac};
        public static int[] store14List = {0xb41, 0xbab, 0xbad, 0xbac};
        public static int[] store15List = {0xb41, 0xbab, 0xbad, 0xbac};
        public static int[] store16List = {0xb41, 0xbab, 0xbad, 0xbac};
        public static int[] store17List = {0xb41, 0xbab, 0xbad, 0xbac};
        public static int[] store18List = {0xb41, 0xbab, 0xbad, 0xbac};
        public static int[] store19List = {0xb41, 0xbab, 0xbad, 0xbac};

        public static int[] store1List =
        {
            0xcdd, 0xcda, 0xccf, 0xcce, 0xc2f, 0xc2e, 0xc23, 0xc19, 0xc11, 0x71f, 0x710,
            0x70b, 0x70a, 0x705, 0x701, 0xcd4, 0x748C
        };

        public static int[] store20List = {0xb41, 0xbab, 0xbad, 0xbac};

        //slime skins
        public static int[] store2List =
        {
            0x227c, 0x227d, 0x258f, 0x2292, 0x2349, 0x234A, 0x234B, 0x234C, 0x234D,
            0x234E, 0x234F, 0x2350, 0x2351, 0x226e
        };

        //bella items/key
        public static int[] store3List = 
        {
            0x228a, 0x228b, 0x228c, 0x228d, 0x228e, 0x228f, 0x2290, 0xa81, 0xa4a
        };

        //abilities
        public static int[] store4List =
        {
            0xa5b, 0xa0c, 0xa30, 0xa55, 0xae1, 0xc58, 0xa65, 0xa6b, 0xaa8, 0xaaf, 0xab6, 0xa46, 0xb20, 0xb32, 0xc58,
            0xad6, 0xa33, 0xa54, 0xa59, 0xa64, 0xa6a, 0xaa7, 0xaae, 0xab5, 0xa45, 0xb1f, 0xb31, 0xc57, 0xa0b
        };

        //armors
        public static int[] store5List =
        {
            0xa96, 0xa95, 0xa94, 0xa60, 0xa93, 0xa92, 0xa91, 0xa13, 0xa90, 0xa8f, 0xa8e, 0xad3
            
        };

        //dagger swords & katana?
        public static int[] store6List =
        {
            0xa87, 0xa86, 0xa85, 0xa07, 0xa8d, 0xa8c, 0xa8b, 0xa1e,
            0xaa2, 0xaa1, 0xaa0, 0xa9f
        };

        //Wands&staves&bows
        public static int[] store7List =
        {
            0xa47, 0xa84, 0xa83, 0xa82, 0xa8a, 0xa89, 0xa88, 0xa19,
            0xc4f, 0xc4e, 0xc4d, 0xc4c
        };

        //Swords&daggers&samurai shit
        public static int[] store8List =
        {
            0xabf, 0xac0, 0xac1, 0xac2, 0xac3, 0xac4, 0xac5, 0xac6, 0xac7, 0xac8, 0xac9,
            0xaca, 0xacb, 0xacc, 0xacd, 0xace
        };

        // rings
        public static int[] store9List = {0xb41, 0xbab, 0xbad, 0xbac};

        private static readonly ILog log = LogManager.GetLogger(typeof (MerchantLists));

        public static void InitMerchatLists(XmlData data)
        {
            log.Info("Loading merchant lists...");
            List<int> accessoryDyeList = new List<int>();
            List<int> clothingDyeList = new List<int>();
            List<int> accessoryClothList = new List<int>();
            List<int> clothingClothList = new List<int>();

            foreach (KeyValuePair<ushort, Item> item in data.Items.Where(_ => noShopCloths.All(i => i != _.Value.ObjectId)))
            {
                if (item.Value.Texture1 != 0 && item.Value.ObjectId.Contains("Clothing") && item.Value.Class == "Dye")
                {
                    prices.Add(item.Value.ObjectType, new Tuple<int, CurrencyType>(51, CurrencyType.Fame));
                    clothingDyeList.Add(item.Value.ObjectType);
                }

                if (item.Value.Texture2 != 0 && item.Value.ObjectId.Contains("Accessory") && item.Value.Class == "Dye")
                {
                    prices.Add(item.Value.ObjectType, new Tuple<int, CurrencyType>(51, CurrencyType.Fame));
                    accessoryDyeList.Add(item.Value.ObjectType);
                }

                if (item.Value.Texture1 != 0 && item.Value.ObjectId.Contains("Cloth") &&
                    item.Value.ObjectId.Contains("Large"))
                {
                    prices.Add(item.Value.ObjectType, new Tuple<int, CurrencyType>(160, CurrencyType.Fame));
                    clothingClothList.Add(item.Value.ObjectType);
                }

                if (item.Value.Texture2 != 0 && item.Value.ObjectId.Contains("Cloth") &&
                    item.Value.ObjectId.Contains("Small"))
                {
                    prices.Add(item.Value.ObjectType, new Tuple<int, CurrencyType>(160, CurrencyType.Fame));
                    accessoryClothList.Add(item.Value.ObjectType);
                }
            }

            ClothingDyeList = clothingDyeList.ToArray();
            ClothingClothList = clothingClothList.ToArray();
            AccessoryClothList = accessoryClothList.ToArray();
            AccessoryDyeList = accessoryDyeList.ToArray();
            log.Info("Merchat lists added.");
        }

        private static readonly string[] noShopCloths =
        {
            "Large Ivory Dragon Scale Cloth", "Small Ivory Dragon Scale Cloth",
            "Large Green Dragon Scale Cloth", "Small Green Dragon Scale Cloth",
            "Large Midnight Dragon Scale Cloth", "Small Midnight Dragon Scale Cloth",
            "Large Blue Dragon Scale Cloth", "Small Blue Dragon Scale Cloth",
            "Large Red Dragon Scale Cloth", "Small Red Dragon Scale Cloth",
            "Large Jester Argyle Cloth", "Small Jester Argyle Cloth",
            "Large Alchemist Cloth", "Small Alchemist Cloth",
            "Large Mosaic Cloth", "Small Mosaic Cloth",
            "Large Spooky Cloth", "Small Spooky Cloth",
            "Large Flame Cloth", "Small Flame Cloth",
            "Large Heavy Chainmail Cloth", "Small Heavy Chainmail Cloth",
        };
    }
}